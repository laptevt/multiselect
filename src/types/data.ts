export type DataType = {
  value: string;
  label: string;
};
