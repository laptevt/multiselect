import React, { useCallback, useEffect, useMemo, useState } from 'react';
import BaseCheckbox from 'src/components/ui/BaseCheckbox';
import BaseLabel from 'src/components/ui/BaseLabel';
import BaseSelect, { BaseSelectProps } from 'src/components/ui/BaseSelect';
import { DataType } from 'src/types/data';

import styles from './MultiSelect.module.scss';

type Props = {
  items: (DataType | string)[];
  value?: (DataType | string)[];
  onChange?: (items: (DataType | string)[]) => void;
} & BaseSelectProps;

const computed = (items: (DataType | string)[]): string[] =>
  items.map((item: DataType | string) => (typeof item === 'object' ? item.label : item));

const MultiSelect: React.FC<Props> = ({ value, onChange, items, ...props }) => {
  const computedItems: string[] = useMemo(() => computed(items), [items]);

  const [selected, setSelected] = useState<number[]>([]);

  useEffect(() => {
    if (Array.isArray(value)) {
      const computedSelected: string[] = computed(value);

      const tempSelected: number[] = [];

      computedItems.forEach((item: string, idx: number) => {
        if (computedSelected.indexOf(item) !== -1) tempSelected.push(idx);
      });

      setSelected(tempSelected);
    }
  }, [value]);

  const returnItems = useCallback(
    (selectedIndexs: number[]): (DataType | string)[] => {
      return selectedIndexs.map((index: number): DataType | string => items[index]);
    },
    [items]
  );

  const changeHandler = useCallback(
    (idx: number) => {
      const select: number = selected.indexOf(idx);
      if (onChange !== undefined) {
        if (select === -1) {
          onChange(returnItems([...selected, idx]));
        } else {
          const temp = [...selected];
          temp.splice(select, 1);

          onChange(returnItems([...temp]));
        }
      } else {
        setSelected((prev) => {
          if (select === -1) {
            return [...prev, idx];
          }
          const temp = [...prev];
          temp.splice(select, 1);

          return temp;
        });
      }
    },
    [selected, onChange, returnItems, setSelected]
  );

  const deleteHandler = useCallback(
    (e: React.MouseEvent, item: string) => {
      e.stopPropagation();
      changeHandler(computedItems.indexOf(item));
    },
    [computedItems, changeHandler]
  );

  const clearHandler = useCallback(() => {
    if (Array.isArray(value) && onChange) onChange([]);
    else setSelected([]);
  }, [value, onChange, setSelected]);

  const header = useCallback(
    () => (
      <div className={styles.multiSelect__wrapper}>
        {selected.map((item: number) => (
          <BaseLabel
            className={styles.multiSelect__label}
            onDelete={(e) => deleteHandler(e, computedItems[item])}
            key={computedItems[item]}
          >
            {computedItems[item]}
          </BaseLabel>
        ))}
      </div>
    ),
    [computedItems, deleteHandler]
  );

  return (
    <BaseSelect header={selected.length ? header() : undefined} onClear={clearHandler} {...props}>
      {computedItems.map((item, idx: number) => (
        <BaseCheckbox
          className={styles.multiSelect__item}
          active={selected.indexOf(idx) !== -1}
          onChange={() => changeHandler(idx)}
          key={item}
        >
          {item}
        </BaseCheckbox>
      ))}
    </BaseSelect>
  );
};

export default React.memo(MultiSelect);
