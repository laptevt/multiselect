import cn from 'classnames';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import { CSSTransition } from 'react-transition-group';

import styles from './BaseSelect.module.scss';

export type BaseSelectProps = {
  placeholder?: string;
  className?: string;
  children?: React.ReactNode;
  empty?: string;
  header?: React.ReactElement;
  absolute?: boolean;
  startOpen?: boolean;
  outsideClose?: boolean;
};

type Props = {
  onClear: (e: React.MouseEvent) => void;
} & BaseSelectProps &
  React.HTMLProps<HTMLDivElement>;

const BaseSelect: React.FC<Props> = ({
  className,
  placeholder,
  children,
  empty,
  header,
  startOpen,
  absolute,
  onClear,
  outsideClose,
  ...props
}) => {
  const [isOpen, setIsOpen] = useState<boolean>(!!startOpen);

  const body = useRef<HTMLDivElement>(null);
  const inner = useRef<HTMLDivElement>(null);
  const root = useRef<HTMLDivElement>(null);
  const clear = useRef<HTMLDivElement>(null);

  const clearHeigth = useCallback<EventListener>((): void => {
    if (body.current) {
      body.current.style.height = '';
    }
  }, []);

  const changeHeight = useCallback((open: boolean): void => {
    if (inner.current && body.current) {
      const margin = Number.parseInt(window.getComputedStyle(inner.current).getPropertyValue('margin-top'), 10);

      body.current.style.height = `${inner.current.offsetHeight + margin}px`;

      if (open) {
        // eslint-disable-next-line @typescript-eslint/no-unused-expressions
        inner.current.offsetHeight;
        body.current.style.height = '';
      }
    }
  }, []);

  const toggleSelect = useCallback((): void => {
    changeHeight(isOpen);
    setIsOpen((prev: boolean): boolean => !prev);
  }, [changeHeight, isOpen, setIsOpen]);

  const clickHandler = useCallback<EventListener>((event: Event) => {
    const target = event.target as HTMLDivElement;

    if (!root.current?.contains(target)) {
      changeHeight(true);
      setIsOpen(false);
    }
  }, []);

  const clearHandler = useCallback((event: React.MouseEvent) => {
    event.stopPropagation();
    onClear(event);
  }, []);

  useEffect(() => {
    if (body.current) {
      body.current.addEventListener('transitionend', clearHeigth);
    }

    if (outsideClose) {
      document.addEventListener('click', clickHandler);
    }

    return () => {
      if (body.current) {
        body.current.removeEventListener('transitionend', clearHeigth);
      }

      if (outsideClose) {
        document.removeEventListener('click', clickHandler);
      }
    };
  }, []);

  return (
    <div
      className={cn(
        styles.baseSelect,
        className,
        absolute && styles['baseSelect--absolute'],
        isOpen && styles['baseSelect--open']
      )}
      ref={root}
      {...props}
    >
      <button type="button" className={styles.baseSelect__head} onClick={toggleSelect}>
        {header ? (
          <>{header}</>
        ) : (
          <p className={styles.baseSelect__placeholder}> {placeholder || 'Выберите необходимые датасеты'}</p>
        )}
        <CSSTransition
          in={!!header}
          timeout={300}
          classNames={{
            enterActive: styles['fade-enter-active'],
            exitActive: styles['fade-exit-active'],
            enterDone: styles['fade-enter'],
            exitDone: styles['fade-exit'],
          }}
          unmountOnExit
          nodeRef={clear}
        >
          {() => <div className={styles.baseSelect__clear} onClick={clearHandler} aria-hidden="true" ref={clear} />}
        </CSSTransition>
      </button>
      <div className={cn(styles.baseSelect__body)} ref={body}>
        <div className={styles.baseSelect__inner} ref={inner}>
          <div className={styles.baseSelect__content}>
            {children || <p className={styles.baseSelect__placeholder}> {empty || 'Нет доступных датасетов'}</p>}
          </div>
        </div>
      </div>
    </div>
  );
};

export default React.memo(BaseSelect);
