import cn from 'classnames';
import React from 'react';

import styles from './BaseLabel.module.scss';

type Props = {
  className?: string;
  children: React.ReactNode;
  onDelete: (e: React.MouseEvent) => void;
} & React.HTMLProps<HTMLDivElement>;

const BaseLabel: React.FC<Props> = ({ className, children, onDelete, ...props }) => (
  <div className={cn(className, styles.baseLabel)} onClick={onDelete} {...props} aria-hidden="true">
    <span className={styles.baseLabel__content}>{children}</span>
    <div className={styles.baseLabel__clear} />
  </div>
);

export default React.memo(BaseLabel);
