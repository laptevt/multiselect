import cn from 'classnames';
import React from 'react';

import styles from './BaseCheckbox.module.scss';

type Props = {
  className?: string;
  children: React.ReactNode;
  active: boolean;
  onChange: (e: React.ChangeEvent) => void;
} & React.InputHTMLAttributes<HTMLInputElement>;

const BaseCheckbox: React.FC<Props> = ({ className, children, active, onChange, ...props }) => (
  <label className={cn(className, styles.baseLabel, active && styles['baseLabel--active'])}>
    <input className={styles.baseLabel__input} type="checkbox" checked={active} onChange={onChange} {...props} />
    <div className={styles.baseLabel__box} />
    <div className={styles.baseLabel__content}>{children}</div>
  </label>
);

export default React.memo(BaseCheckbox);
