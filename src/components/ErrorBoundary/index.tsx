import React, { Component, ReactChild, ReactChildren } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';

type ErrorBoundaryProps = {
  children: ReactChild | ReactChildren;
} & RouteComponentProps;

class ErrorBoundary extends Component<ErrorBoundaryProps, { hasError: boolean }> {
  constructor(props: ErrorBoundaryProps) {
    super(props);
    this.state = { hasError: false };
  }

  render() {
    const { children } = this.props;
    const { hasError } = this.state;

    if (hasError) {
      return <div>Что-то пошло не так...</div>;
    }

    return children;
  }
}

export default withRouter(ErrorBoundary);
