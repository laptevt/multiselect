import axios from 'axios';
import React, { useCallback, useEffect, useState } from 'react';
import Multiselect from 'src/components/ui/MultiSelect';
import { DataType } from 'src/types/data';

type ResponseType = {
  link: string;
  title: string;
};

const Page: React.FC = () => {
  const [data, setData] = useState<DataType[]>([]);

  const [selected, setSelected] = useState<(DataType | string)[]>([]);

  useEffect(() => {
    (async () => {
      const response = await axios.get(
        'https://xn--80adgxdjdid1ar3isb.xn--p1ai/api/resources?categories_like=education&audiences_like=children&_page=1'
      );
      setData(
        response.data.map(
          (item: ResponseType, idx: number): DataType => ({ label: `${item.title}-${idx}`, value: item.link })
        )
      );
    })();
  }, []);

  const onChange = useCallback(
    (items: (DataType | string)[]): void => {
      setSelected(items);
    },
    [setSelected]
  );

  return (
    <section className="page">
      <h2>Датасеты</h2>
      <Multiselect className="page__select" startOpen items={data} onChange={onChange} value={selected} outsideClose />
      <Multiselect className="page__select" startOpen items={data.map((item) => item.label)} />
    </section>
  );
};

export default Page;
